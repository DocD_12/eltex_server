/* 
 * File:   main.c
 * Author: asus
 *
 * Created on 16 Декабрь 2015 г., 18:15
 */

#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */


void DieWithError(char *errorMessage)  
{
    perror(errorMessage);
    exit(1);
};  

int main(int argc, char *argv[])
{
    int sock;                        
    struct sockaddr_in Addr;     
    unsigned short Port;     
    char *servIP;                    
    int sl;           
    char buf[1024];
    int n = 0;

    servIP = "127.0.0.1";    
    Port = 2048; 

    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        DieWithError("socket() failed");

    memset(&Addr, 0, sizeof(Addr));   
    Addr.sin_family = AF_INET;                 
    Addr.sin_addr.s_addr = inet_addr(servIP); 
    Addr.sin_port   = htons(Port);   

    if (bind(sock, (struct sockaddr *)&Addr, sizeof(Addr)) < 0)
        DieWithError("bind() failed");
    
    
    while(1)
    {
        sl = recvfrom(sock, buf, 1024, 0, NULL, NULL);
        buf[sl] = '\0';
        printf("%s:%d - %d\n", &buf, sl, n);
        n++;
    }
    
    return 0;
}
