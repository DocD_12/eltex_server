/* 
 * File:   main.c
 * Author: Artem Davletshin
 *
 * Created on 15 Декабрь 2015 г., 22:36
 */

#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define ECHOMAX 255     /* Longest string to echo */

void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
};  

int main(int argc, char *argv[])
{
    int sock;                        /* Socket */
    struct sockaddr_in echoServAddr; /* Local address */
    char* mesReady;        /* Buffer for echo string */
    unsigned short echoServPort;     /* Server port */

    echoServPort = 2048;  /* local port */
    mesReady = "Can read message";
        
    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        DieWithError("socket() failed");

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK); /* Any incoming interface */
    echoServAddr.sin_port = htons(echoServPort);      /* Local port */
    
    for (;;) 
    {
        if (sendto(sock, mesReady, strlen(mesReady), 0, (struct sockaddr *)&echoServAddr, sizeof(echoServAddr)) < 0)
            DieWithError("sendto() failed");
        sleep(2);
    }
}


